# Nginx reverse proxy for WebIS 

Fork https://gitlab.cern.ch/rhauser/webis-nginx and 
modify `index.html` and `nginx.conf` as needed.

```shell
oc new-project webis
oc new-app centos/nginx-116-centos7~https://gitlab.cern.ch/rhauser/webis-nginx
oc status
```

Note: replace the URL to the git repo with your own.

## CERN SSO Proxy

Either install interactively on the web console, or, if you have
`helm` installed:

```shell
helm repo add https://registry.cern.ch/chartrepo/paas-helm-charts
helm show values paas/cern-auth-proxy
helm install cern-proxy paas/cern-auth-proxy --set upstream.service.name=webis-nginx --set upstream.service.port=8080 --set route.hostname=atlas-tdaq-webis.app.cern.ch
```

## Modifications

After modifications to `index.html` or `nginx.conf`, push to 
your repository and 

```shell
oc start-build webis-nginx
```
